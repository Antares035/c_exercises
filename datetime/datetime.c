#include <stdio.h>
#include <stdlib.h>

#include "julian.h"

int main(void)
{
    char normal[9];
    printf("输入日期：");
    scanf("%s", normal);

    char julian[] = "0000000";
    normalToJulian(normal, julian, 'A');
    printf("Julian日期：%s", julian);
    
    return 0;
}
