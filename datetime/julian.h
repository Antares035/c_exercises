#ifndef JULIAN_H_
#define JULIAN_H_

#include <string.h>

/*
判断平年和闰年，平年返回0，闰年返回1
*/
int checkLeapYear(const char* year){
	int yyyy = atoi(year);
	if (yyyy % 100 == 0){
    	if (yyyy % 400 == 0){
    		return 1;
    	}
    	else{
    		return 0;
    	}
    }
    else{
    	if (yyyy % 4 == 0){
    		return 1;
    	}
    	else{
    		return 0;
    	}
    }
}


/*
把普通日期格式转换成Julian日期格式
*/
int normalToJulian(const char* normal, char* julian, char type){
	// 输入年月日
	char year[] = "0000";
	memcpy(year, normal, 4);
	int yyyy = atoi(year);

	char month[] = "00";
	memcpy(month, normal + 4, 2);
	int mm = atoi(month);

	char day[] = "00";
	memcpy(day, normal + 6, 2);
	int dd = atoi(day);

    int ddd = 0;

    // 如果是按照实际天数计算
	if (type == 'A'){
		int dayTable[] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};

    	// 获取月份对应的天数
    	ddd = dayTable[mm - 1];

		// 加上当月里的天数
    	ddd = ddd + dd;

    	// 如果是闰年，并且月份是三月及以后的，天数增加1
    	if(checkLeapYear(year)){
			if (mm > 1){
            	ddd = ddd + 1;
			}    	
    	}
	}
	// 如果是按照30天计算
	else {
		ddd = (mm - 1) * 30;

		// 如果是闰年，并且是2月，并且当天是29号，则视为30天
		if (checkLeapYear(year)){
			if (mm == 2 && dd == 29){
				dd = 30;
			}
		}
		// 如果是平年，并且是2月，并且当天是28号，则视为30天
		else {
			if (mm == 2 && dd == 28){
				dd = 30;
			}
		}
		
		ddd = ddd + dd;
	}

	// 输出结果
	memcpy(julian, year, 4);
    sprintf(julian + 4, "%03d", ddd);

    return yyyy * 1000 + ddd;
}


/*
按实际天数把Julian日期格式转换成普通日期格式
*/
int julianToNormal(const char* julian, char* normal){
	// julian年份
	char year[] = "0000";
	memcpy(year, julian, 4);

    // julian天数
	char day[] = "000";
	memcpy(day, julian + 4, 3);

    // 定义天数表
    int dayTable[] = {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335};

    // 如果是闰年，天数表三月份及以后的月份增加1
	if (checkLeapYear(year)){
        for (int i = 2; i < 12; i++){
			dayTable[i] = dayTable[i] + 1;
		}
	}

    // 定位julian天数所在月份
    int ddd = atoi(day);
	int sub = 0;
	for (int i = 11; i >= 0; i--){
		if (ddd >= dayTable[i]){
			sub = i;
			break;
		}
	}

    // 得到年月日的值
	int mm = sub + 1;
	int dd = ddd - dayTable[sub];
	int yyyy = atoi(year);

	// 输出结果
    sprintf(normal, "%04d", yyyy);
	sprintf(normal + 4, "%02d", mm);
	sprintf(normal + 6, "%02d", dd);

    return yyyy * 10000 + mm * 100 + dd;
}


#endif /* JULIAN_H_ */
